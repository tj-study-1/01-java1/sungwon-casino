package sungwon2.casino.game;

import java.util.Scanner;

import sungwon2.casino.player.Player;
import sungwon2.casino.view.PrintScreen;

public class EvenOddGame extends Game{

	
	@Override
	public void rule() {
		System.out.println();
		System.out.println();
		System.out.println();
		
		System.out.println("====================");
		System.out.println("홀짝게임은 배팅금액을 입력한 뒤.");
		System.out.println("홀 짝중 하나를 입력합니다");
		System.out.println("입력하신게 맞으면 배팅금액의 두배를얻고");
		System.out.println("틀렸다면 배팅금액을 잃습니다");
	}

	@Override
	public void playScreen() {	
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		
		System.out.println("====================");
		System.out.println("홀짝게임입니다.");
		System.out.println("1.설명 보기.");
		System.out.println("2.현재 칩 갯수.");
		System.out.println("3.게임하기.");
		System.out.println("4. 종 료.");
	}
	
	@Override
	public void play() {
		Player player = Player.getPlayer();
		PrintScreen ps = new PrintScreen();
		Scanner sc = new Scanner(System.in);
		
		System.out.print("배팅 금액을 입력하세요");
		int bet = sc.nextInt();
		if( bet > player.getChip()) {
			System.out.println("칩이 없습니다");
			return;
		}
		System.out.print("홀 짝중 하나를 입력하세요");
		char ch = sc.next().charAt(0);
		int random = (int) (Math.random() * 2);
		if(random % 2 == 0 && ch =='짝') {		
			System.out.println("맞췄습니다!!");
			player.setChip(player.getChip() + bet);
		} else if(random % 2 == 1 && ch == '홀') {
			System.out.println("맞췄습니다!!");
			player.setChip(player.getChip() + bet);
		} else {
			System.out.println("틀렸습니다 !!!");
			player.setChip(player.getChip() - bet);
		}	
		ps.playerInformation();
		
	}
	
	
}
