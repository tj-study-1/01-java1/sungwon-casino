package sungwon2.casino.menu;

import java.util.Scanner;
import sungwon2.casino.game.EvenOddGame;
import sungwon2.casino.game.Game;
import sungwon2.casino.player.Player;
import sungwon2.casino.view.PrintScreen;

public class CasinoMenu{
	
	public CasinoMenu() {
		Player player = Player.getPlayer();
		PrintScreen ps = new PrintScreen();
		ps.printWelcome();
		player.setNameAndChip();
	}

	public void mainMenu() {
		Scanner sc = new Scanner(System.in);
		PrintScreen ps = new PrintScreen();
		while(true) {
			
			ps.printMainMenu();
			
			int no = sc.nextInt();
			
			switch(no) {
				case 1: ps.playerInformation(); break;
				case 2: evenOddMenu(); break;
				case 3: 
				case 9: break;
				default :
					System.out.println("잘못된 값을 입력했습니다.");
			}
			if(no == 9) {
				break;
			}
		}
	}
	
	public void evenOddMenu() {
		PrintScreen ps = new PrintScreen();
		Game game = new EvenOddGame();
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			game.playScreen();
			System.out.print("메뉴선택 : ");
			int no = sc.nextInt();
			switch(no) {
				case 1: game.rule();; break;
				case 2: ps.playerInformation(); break;
				case 3: game.play();
				case 4: break;
				default :
					System.out.println("잘못된 값을 입력했습니다.");
			}
			if(no == 4) {
				break;
			}
			
		}
	}
	
	
	
	
}
