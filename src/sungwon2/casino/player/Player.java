package sungwon2.casino.player;

import java.util.Scanner;

public class Player {
	
	private static Player player = new Player();
	
	public static Player getPlayer() {
		return player;
	}
	
	
	private static int chip;
	private static String name;
	
	public static int getChip() {
		return chip;
	}

	public static void setChip(int chip) {
		
		if(chip > 0) {
			player.chip = chip;
		} else {
			System.out.println("플레이어의 칩이 모자랍니다.");
			player.chip = 0;
		}
	}

	public static String getName() {
		return name;
	}

	public static void setName(String name) {
		Player.name = name;
	}

	public void setNameAndChip() {
		Scanner sc = new Scanner(System.in);
		System.out.print("이름 : ");
		name = sc.nextLine();
		System.out.print("칩 갯수 : ");
		chip = sc.nextInt();
		
	}
	
	
	
}
